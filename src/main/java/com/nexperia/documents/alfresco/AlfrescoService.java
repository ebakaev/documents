package com.nexperia.documents.alfresco;

import com.nexperia.documents.model.Constants;
import com.nexperia.documents.model.NexperiaDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AlfrescoService {

    private final AlfrescoIntegrationService alfrescoIntegrationService;

    @Autowired
    public AlfrescoService(AlfrescoIntegrationService alfrescoIntegrationService) {
        this.alfrescoIntegrationService = alfrescoIntegrationService;
    }

    public String getTicket(Credentials credentials) {
        String ticketResponse = alfrescoIntegrationService.getTicket(credentials, String.class);
        return parseTicket(ticketResponse);
    }

//    private static Map<String, String> getAuthParams(Credentials creds) {
//        return Map.of("u", creds.getUsername(), "pw", URLEncoder.encode(creds.getPassword(), StandardCharsets.UTF_8));
//    }

    private static String parseTicket(String ticketXML) {
        return ticketXML.substring(ticketXML.indexOf("<ticket>") + "<ticket>".length(), ticketXML.indexOf("</ticket>"));
    }

    public List<NexperiaDocument> findDocuments(String nexperiaDocumentName) {
        String alfTicket = getTicket(new Credentials("admin", "Nex@dmin"));
        Object[] spiderLibraryDocumentsObjects = alfrescoIntegrationService.get(nexperiaDocumentName, alfTicket, Object[].class);
        List<NexperiaDocument> spiderLibraryDocuments = new ArrayList<>();
        for (Object document: spiderLibraryDocumentsObjects) {
            Map<String, String> doc = (Map<String, String>) document;
            NexperiaDocument nexperiaDocument = new NexperiaDocument();
            nexperiaDocument.setPublicationId(doc.get(Constants.FILE_ID));
            nexperiaDocument.setName(doc.get(Constants.FILE_NAME));
            nexperiaDocument.setDescription(doc.get(Constants.DESCRIPTION));
            nexperiaDocument.setDescriptiveTitle(doc.get(Constants.DESCRIPTIVE_TITLE));
            nexperiaDocument.setDocumentType(doc.get(Constants.FILE_TYPE));
            nexperiaDocument.setVersion(doc.get(Constants.VERSION));
            nexperiaDocument.setNodeUUID(doc.get(Constants.NODE_UUID));
            nexperiaDocument.setTitle(doc.get(Constants.TITLE));
            nexperiaDocument.setSystem("SPIDER Library");
            nexperiaDocument.setPtw(doc.get(Constants.PTW).equals(Boolean.TRUE.toString()) ? "Yes" : "No");
            nexperiaDocument.setDownloadUrl(String.format(Constants.SPIDER_LIBRARY_DOWNLOAD_FORMAT, doc.get(Constants.NODE_UUID), doc.get(Constants.FILE_NAME), alfTicket));
            nexperiaDocument.setViewUrl(String.format(Constants.SPIDER_LIBRARY_DOWNLOAD_FORMAT, doc.get(Constants.NODE_UUID), doc.get(Constants.FILE_NAME), alfTicket).replace("/a/", "/d/"));
            spiderLibraryDocuments.add(nexperiaDocument);
        }

        return spiderLibraryDocuments;
    }
}
