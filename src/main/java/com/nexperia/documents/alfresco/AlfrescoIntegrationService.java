package com.nexperia.documents.alfresco;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class AlfrescoIntegrationService {
    private final RestTemplate restTemplate;

    private static final Map<String, String> URL_BAD_SYMBOLS_REPLACE = Map.of(
            "&", "%26",
            "#", "%23"
    );

    @Value("${app.alfresco.spider.url}")
    private String alfrescoSpiderUrl;

    private static final String ENDPOINT_APP = "/s";

    public AlfrescoIntegrationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T get(String documentName, String ticket, Class<T> type) {
        return restTemplate.getForEntity(buildSpiderSearchURL(documentName, ticket), type).getBody();
    }

    private String buildSpiderSearchURL(String documentName, String ticket) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoSpiderUrl)
                .append(ENDPOINT_APP)
                .append("/nxp/spc/search?properties=FildID,Title,FileType,FileName,Version,Description,DescriptiveTitle,NodeUUID,PublishedOnWeb")
                .append("&FileName=")
                .append(documentName)
                .append("&alf_ticket=")
                .append(ticket);
        return url.toString();
    }

    public <T> T getTicket(Credentials credentials, Class<T> type) {
        return restTemplate.getForEntity(buildAlfTicketURL(credentials), type).getBody();
    }

    private String buildAlfTicketURL(Credentials credentials) {
        StringBuilder url = new StringBuilder()
                .append(alfrescoSpiderUrl)
                .append(ENDPOINT_APP)
                .append("/api/login?")
                .append("u=")
                .append(credentials.getUsername())
                .append("&pw=")
                .append(credentials.getPassword());
        return url.toString();
    }
}
