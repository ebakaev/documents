package com.nexperia.documents.service;

import com.nexperia.documents.alfresco.AlfrescoService;
import com.nexperia.documents.model.NexperiaDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpiderLibraryDocumentService {

    private final AlfrescoService alfrescoService;

    @Autowired
    public SpiderLibraryDocumentService(AlfrescoService alfrescoService) {
        this.alfrescoService = alfrescoService;
    }

    public List<NexperiaDocument> findNexperiaDocumentByName(String nexperiaDocumentName) {
        System.out.println("Searching for document in all systems: " + nexperiaDocumentName);
        //Spider Library documents
        List<NexperiaDocument> spiderLibraryDocuments = alfrescoService.findDocuments(nexperiaDocumentName);
        //AWS documents
        //CCM documents ?
        //TDM documents ?
        //???
        List<NexperiaDocument> searchResult = new ArrayList<>();
        searchResult.addAll(spiderLibraryDocuments);
        return searchResult;
//        return nexperiaDocumentRepo.findAll();
    }

}
