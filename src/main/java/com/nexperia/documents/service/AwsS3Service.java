package com.nexperia.documents.service;

import com.nexperia.documents.model.NexperiaDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Service
public class AwsS3Service {

    private S3Client s3;

    @Value("${app.aws.s3.bucketName}")
    String bucketName;

    public AwsS3Service(@Value("${app.aws.s3.accessKeyId}") String accessKeyId, @Value("${app.aws.s3.secretKeyId}") String secretKeyId, @Value("${app.aws.s3.region}") String regionString) {
        Region region = Region.of(regionString);
        AwsCredentials credentials = AwsBasicCredentials.create(accessKeyId, secretKeyId);
        AwsCredentialsProvider credentialsProvider = StaticCredentialsProvider.create(credentials);
        s3 = S3Client.builder()
                .credentialsProvider(credentialsProvider)
                .region(region)
                .build();
    }

    public List<NexperiaDocument> findDocuments(String searchString) {
        List<NexperiaDocument> result = new ArrayList<>();
        try {
            ListObjectsRequest listObjects = ListObjectsRequest
                    .builder()
                    .bucket("nexperia-pws-data-qa")
                    .build();

            ListObjectsResponse res = s3.listObjects(listObjects);
            List<S3Object> objects = res.contents();

            for (ListIterator iterVals = objects.listIterator(); iterVals.hasNext(); ) {
                S3Object myValue = (S3Object) iterVals.next();
                String documentKey = myValue.key();
                if (documentKey.contains("documents/drawings/PtW") && documentKey.contains(searchString)) {
                    System.out.print("\nFound by search string: " + documentKey.replace("documents/drawings/PtW/", ""));
                    NexperiaDocument document = new NexperiaDocument();
                    document.setName(documentKey.replace("documents/drawings/PtW/", ""));
                    document.setSystem("AWS");
                    document.setAwsKeyName(documentKey);
                    result.add(document);
                }
            }
        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }

        return result;
    }

    public Object download(String keyName) {
        try {
            GetObjectRequest objectRequest = GetObjectRequest
                    .builder()
                    .key(keyName)
                    .bucket(bucketName)
                    .build();

            ResponseBytes<GetObjectResponse> objectBytes = s3.getObjectAsBytes(objectRequest);
            byte[] data = objectBytes.asByteArray();

            // Write the data to a local file
            File myFile = new File("g:\\temp\\aws\\" + keyName.replace("documents/drawings/PtW/", ""));
            OutputStream os = new FileOutputStream(myFile);
            os.write(data);
            System.out.println("\nSuccessfully obtained bytes from an S3 object");
            os.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }

        return null;
    };

}
