package com.nexperia.documents.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class NexperiaDocument {

    private String name;
    private String title;
    private String description;
    private String descriptiveTitle;
    private String documentType;
    private String publicationId;
    private String version;
    private String ptw;
    private String system;
    private String nodeUUID;
    private String downloadUrl;
    private String viewUrl;

    private String awsKeyName;

}
