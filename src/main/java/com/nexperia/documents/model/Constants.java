package com.nexperia.documents.model;

public interface Constants {

    String FILE_ID = "FileID";
    String TITLE = "Title";
    String FILE_TYPE = "FileType";
    String FILE_NAME = "FileName";
    String VERSION = "Version";
    String DESCRIPTION = "Description";
    String DESCRIPTIVE_TITLE = "DescriptiveTitle";
    String NODE_UUID = "NodeUUID";
    String PTW = "PublishedOnWeb";

    String SPIDER_LIBRARY_DOWNLOAD_FORMAT = "https://qa.publish-dam.pim.nexperia.com/d/a/workspace/SpacesStore/%s/%s?ticket=%s";
}
