package com.nexperia.documents;

import com.nexperia.documents.model.NexperiaDocument;
import com.nexperia.documents.service.AwsS3Service;
import com.nexperia.documents.service.SpiderLibraryDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/document")
public class NexperiaDocumentController {

    private final SpiderLibraryDocumentService spiderLibraryDocumentService;
    private final AwsS3Service awsS3Service;

    @Autowired
    public NexperiaDocumentController(SpiderLibraryDocumentService spiderLibraryDocumentService, AwsS3Service awsS3Service) {
        this.spiderLibraryDocumentService = spiderLibraryDocumentService;
        this.awsS3Service = awsS3Service;
    }

    @GetMapping("/all")
    public ResponseEntity<List<NexperiaDocument>> getAllNexperiaDocuments() {
        List<NexperiaDocument> documents = spiderLibraryDocumentService.findNexperiaDocumentByName("");
        return new ResponseEntity<>(documents, HttpStatus.OK);
    }

    @Validated
    @GetMapping("/search/{documentName}")
    public ResponseEntity<List<NexperiaDocument>> getNexperiaDocumentById(@PathVariable("documentName") String documentName) {
        List<NexperiaDocument> spiderLibraryDocuments = spiderLibraryDocumentService.findNexperiaDocumentByName(documentName);
        List<NexperiaDocument> awsDocuments = awsS3Service.findDocuments(documentName);

        List<NexperiaDocument> resultListOfDocuments = new ArrayList<NexperiaDocument>();
        resultListOfDocuments.addAll(spiderLibraryDocuments);
        resultListOfDocuments.addAll(awsDocuments);

        return new ResponseEntity<>(resultListOfDocuments, HttpStatus.OK);
    }
}
