package com.nexperia.documents.exception;

public class NexperiaDocumentNotFoundException extends RuntimeException {
    public NexperiaDocumentNotFoundException(String message) {
        super(message);
    }
}
